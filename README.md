# Mellona - Swarm Simulation Tool

A general swarm simulation tool that can be tailored to your needs.


## Summary

## Installation

First ensure git is installed on your machine and clone the repository:
> `git clone https://<your username>@bitbucket.org/hauertlab/mellona.git`

for http or

> `git clone git@bitbucket.org:hauertlab/mellona.git`

for ssh,

Then it is recommended you either set up conda or virtualenv to contain the project dependencies. This project uses python3.5 or greater. Ensure pip is installed.

The requirements are listed within setup.py:
> `pip install numpy overrides matplotlib`

Note: Must check if `pip install -e .` works as an installer

Then in any external project you can `import mellona` to access this projects modules

Note, if not installed, you will have to add a path to mellona into `sys.path`, i.e.
```python
import sys
sys.path.append('relative-path-to-mellona-root-directory')
import mellona
```

## Project Structure

The Mellona Project Source files are all stored in [mellona](mellona) and comprises of several submodules.
```
├── README.md
├── docs
├── example
├── mellona
│   ├── __init__.py
│   ├── core
│   │   ├── __init__.py
│   │   ├── base_agents.py
│   │   ├── base_controller.py
│   │   ├── base_environment.py
│   │   ├── base_sensor.py
│   │   ├── entity.py
│   │   ├── properties.py
│   │   └── shape.py
│   ├── entities
│   │   ├── __init__.py
│   │   ├── agents
│   │   ├── dynamicobjects
│   │   └── staticobjects
│   ├── environment
│   │   └── __init__.py
│   ├── sensors
│   │   └── __init__.py
│   ├── tests
│   │   ├── __init__.py
│   │   └── test_entity.py
│   ├── utils
│   │   ├── __init__.py
│   │   └── logging.py
│   └── visualisation
│       └── __init__.py
└── setup.py
```
(Generate using the `tree -A -L 3` tool on linux)

- The `core` module contains the Abstract classes and core elements of the simulation engine
- The `entities` module contains Implementations of Abstract Entity Classes
    - The `agents` module contains ready made set of controllable agents
    - The `dynamicobjects` module contains implementations of interactable objects
    - The `staticobjects` module contains implemetnations of static objects such as walls
- The `environment` module contains implemented environment types
- The `sensors` module contains implemented sensors to attach to agents
- The `visulisations` moudle contains simple ways of visualising a given controller
- The `utils` module contains data processing and logging methods.

## System Architecture

This project follows an object oriented approach as best of possible. This includes both the use of:

1. Abstract Base Classes from the [`abc` library](https://docs.python.org/3/library/abc.html) to enforce subclass function checks at runtime. This is used in conjunction with the [`overrides` annotation](https://pypi.org/project/overrides/) to enforce proper inheritence and class extension.
2. Typing from the [`typing` python module](https://docs.python.org/3/library/typing.html). Enforcing Static Typing in Python may be a pain, but the larger the project, the stricter you have to be with being consistent in types. Recommendation is to use this in conjunction with the [`mypy` python linter](https://mypy.readthedocs.io/en/stable/) in [VSCode](https://mypy.readthedocs.io/en/stable/) or Pycharm or whichever IDE you use.

The high level picture is the following:

![text](docs/images/RelationshipDiagram.png)