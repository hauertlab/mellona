from setuptools import setup

setup(
    name='mellona',
    version='0.1',
    description='Swarm Simulator',
    url='',
    author='',
    author_email='',
    license='MIT',
    packages=['mellona'],
    install_requires=[
        'overrides',
        'numpy'
    ],
    zip_safe=False
)