import abc
import typing

# TODO Figure out if these two can be effectively merged?

class Collidable(abc.ABC):
    """This abstract class must be inherited from if the entity is collidable. This is the Dynamic Entity, Static Entity and its subclasses.

    # TODO Determine what functions are required to be implmented for any collidable object
    """
    _collisions_enabled: bool

    def enable_collisions(self, enable:bool):
        _collisions_enabled = enable


class PhysicsObject(abc.ABC):
    """This abstract class must be inherited from if the entity follows physics. This is the Dynamic Entity and its subclasses.

    This may be to determine factors such as gravity, momentum, collision impulses and so on.

    # TODO Determine what functions are required to be implmented for any physics object
    """

    _physics_enabled: bool

    def enable_physics(self, enable:bool):
        _physics_enabled = enable

    @abc.abstractmethod
    def tick_physics(self):
        pass