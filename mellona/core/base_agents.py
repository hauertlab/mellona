import abc
import typing

from .entity import ControllableEntity
from .base_sensor import BaseSensor
from .base_environment import BaseEnvironment

class BaseAgent(ControllableEntity):
    """Abstract Base Class describin the functionality that all agents need to have

    The agent contains all the functioality of the controllable entity and must implement all abstract methods.

    The BasicAgent is an controllable entity with sensors

    Args:
        ControllableEntity ([type]): [description]
    """

    def __init__(self, environment:BaseEnvironment, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._sensors: dict[str, BaseSensor] = {}
        self._environment: BaseEnvironment = environment

    def register_sensor(self, sensor:BaseSensor):
        self._sensors[sensor.uid] = sensor