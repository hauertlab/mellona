import abc
from typing import List

from .entity import DynamicEntity, Entity
from .base_environment import BaseEnvironment

class BaseController(abc.ABC):
    """Abstract Class describing minimum functionality that all controllers need to have.

    Minimum functionality includes a list of dynamic entitiees, a reference to the environmen

    Any subvlass must implement the reigster, initialise and run functions.

    Args:
        abc ([type]): [description]
    """

    _dynamic_entities: List[DynamicEntity]
    _environment: BaseEnvironment

    def __init__(self, environment: BaseEnvironment):
        self._dynamic_entities = []
        self._environment = environment

    @abc.abstractmethod
    def register(self, entity: Entity):
        """General function which registers any entity to the controller.

        If the entity is a dynamic entity, this class stores the instance to use in the main program loop. All entities are stored in the environment for collision checking.

        Unless this paradigm is changed in subclasses, this behaviour can be run by calling super().register(entity) in the body of the subclass

        Args:
            entity (Entity): Entity to be registered to this controller
        """
        self._environment.register(entity)
        if issubclass(type(entity), DynamicEntity):
            self._dynamic_entities.append(entity)

    @abc.abstractmethod
    def initialise(self):
        """Function which does any internal initialisation before running. Called by the user.
        """
        pass

    @abc.abstractmethod
    def run(self, iterations:int):
        """Function which performs the main simulation loop and calls tick on all the DynamicAgents and PhysicsAgents.
        pass