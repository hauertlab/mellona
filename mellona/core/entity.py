import abc
import typing
import numpy as np

from .properties import Collidable, PhysicsObject
from .shape import Shape

class Entity(abc.ABC):
    """Core Abstract Class describing any object within the simulation with size, shape and a location.

    All Subclasses of this class are stored by the environment.

    #TODO Add any extra core properties that every entity should have
    """

    def __init__(self, uid: str, shape: Shape, location: np.ndarray):
        self._uid = uid
        self._shape = shape
        self._location = location

    @property
    def uid(self) -> str:
        return self._uid

    @property
    def shape(self) -> np.ndarray:
        return self._shape

    @property
    def location(self) -> np.ndarray:
        return self._location

    @location.setter
    def location(self, location):
        self._location = location


class StaticEntity(Entity, Collidable):
    """Abstract Class which describes a collidable object which does not move

    This class contains all the functions of both Entity and Collidable Parent Classes

    Implementing items such as Walls, Columns, Bodies of Water should extend this class.

    Note: As any static entity does not move, it is not considered a physics object, but may effect other physics object entities.

    #TODO Add any extra functionality
    """
    pass


class DynamicEntity(Entity, PhysicsObject, Collidable):
    """Abstract Class describing any entity which moves and obeys physics

    This class Contains all the functions of Entity, Collidable and PhysicsObject Parent Classes

    Implementing interactable items, but NOT user controlled items, for example crates, tables, luggage

    All subclasses must implement the tick function which is run every loop, this tick may wish to increment num_ticks
    """
    def __init__(self, num_ticks:int):
        self._num_ticks = 0

    @abc.abstractmethod
    def tick(self):
        pass


class ControllableEntity(DynamicEntity):
    """Abstract Class describing any entity which the user can control, or contains its own processing logic and obeys physics.

    This class Contains all the functionalasity of DynamicEntity

    Implementing Robots, Fire Spread, Human Interaction should extend this class

    # TODO Add any extra functionality
    """
    pass