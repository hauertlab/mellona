import abc
import typing
import numpy as np

from .base_environment BaseEnvironment

class BaseSensor(abc.ABC):
    """Abstract Sensor Class

    This describes the minimum requirements and functioality for every implemented Sensor.

    The sensor is required to have a uid, a transformation matric w.r.t the body on which this sensor is attached, and a reference to the environment in which it can query.

    # TODO What is the minimum functions?
    """

    def __init__(self, uid: str, sensor2body_transform: np.ndarray, environment : BaseEnvironment):
        self._uid = uid
        self._sensor2body_transform: np.ndarray
        self._environment = environment

    @property
    def uid(self):
        return self._uid