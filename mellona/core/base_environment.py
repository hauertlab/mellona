import abc
from typing import List, Dict

from .entity import Entity

class BaseEnvironment(abc.ABC):
    """Abstract Class of the Environment
    This describes the minimum requirements and functioality for every implemented environment.

    # TODO Minimum functionality needs to be agreed upon
    """

    def __init__(self):
        _entities: Dict[str, Entity] = {}

    def register_entity(self, entity:Entity):
        _entities[entity.uid] = entity