import numpy as np

class Shape(object):
    """A class which describes the shape of an entity.

    The shape may be inputted in many ways, e.g. as a numpy array, as geometric primitives, as a mesh etc etc.

    This class must then convert those representations into one which is optimised for bounds and collision checking.

    # TODO Work out what is required inside here and the required interface
    """

    def __init__(self, uid:str):
        self._uid = uid

    def set_shape_as_numpy(self, points: np.ndarray):
        # TODO do something with points to store as shape
        pass