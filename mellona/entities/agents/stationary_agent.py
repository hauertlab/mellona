from ...core import BaseAgent

from overrides import overrides


class StationaryAgent(BaseAgent):
    """An example class demonstrating a stationary non-moving agent

    Properties:
        _uid: str <- Entity
        _shape: Shape <- Entity
        _location: np.ndarray <- Entity
        _collisions_enabled: bool <- Collidable
        _physics_enabled: bool <- PhysicsObject
        _num_ticks: int <- DynamicEntity
        _sensors: List[Sensor] <- BaseAgent
        _environment: BaseEnvironment <- BaseAgent
    """

    @overrides
    def tick(self):
        pass


    @overrides
    def tick_physics(self):
        pass