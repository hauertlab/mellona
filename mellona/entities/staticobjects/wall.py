from ...core import StaticEntity

from overrides import overrides


class Wall(StaticEntity):
    """An example class demonstrating a basic static object

    Perhaps 'Structure' might be a better name? this class describes the main outer walls of the environment.

    Properties:
        _uid: str <- Entity
        _shape: Shape <- Entity
        _location: np.ndarray <- Entity
        _collisions_enabled: bool <- Collidable

    # TODO Complete implementation
    """
    pass