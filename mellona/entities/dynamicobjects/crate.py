from ...core import DynamicEntity

from overrides import overrides


class Crate(DynamicEntity):
    """An example class demonstrating a basic dynamic object

    Properties:
        _uid: str <- Entity
        _shape: Shape <- Entity
        _location: np.ndarray <- Entity
        _collisions_enabled: bool <- Collidable
        _physics_enabled: bool <- PhysicsObject
        _num_ticks: int <- DynamicEntity

    # TODO Complete implementation
    """

    @overrides
    def tick(self):
        pass


    @overrides
    def tick_physics(self):
        pass