import unittest
import numpy as np
import numpy.testing as npt

from ..core import *

class TestEntityMethods(unittest.TestCase):

    def setUp(self):
        self.shape = np.array([[0, 1, 2],[2, 3, 4], [3, 4, 5]])
        self.location = np.array([0, 1, 2])
        self.entity = Entity(self.shape, self.location)

    def test_set_shape(self):
        npt.assert_equal(self.entity.shape, self.shape, "Entity shape not properly set")

    def test_set_location(self):
        new_loc = np.array([0, 1, 2])
        self.entity.location = new_loc
        npt.assert_equal(self.entity.location, new_loc, "Entity location not properly set")